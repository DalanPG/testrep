*** Settings***
Library     String
Library     SeleniumLibrary

***Variables***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}       http
${testURL}      ${scheme}://${homepage}

***Keywords***
Open homepage
    Open browser    ${testURL}      ${browser}

***Test Cases***
C001 Hacer click en contenedores
    Open homepage
    Set Global variable     @{NombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${ItemEnArray}  IN      @{NombresDeContenedores}
    \   Wait Until Element Is Visible  xpath=${ItemEnArray}
    \   Click Element   xpath=${ItemEnArray}
    \   Wait Until Element Is Visible  xpath=//*[@id="bigpic"]
    \   Click Element   xpath=//*[@id="header_logo"]/a/img
    Close Browser
